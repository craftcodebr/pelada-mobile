/**
 * Created by icastilho on 9/30/15.
 */
angular.module('starter.controllers.pelada', [])

   .controller('PeladasCtrl', function ($rootScope, $scope,$stateParams, Pelada ) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onActiveCtrl();
         console.log('Peladas Entro');
         loadPelada();
      });

      $scope.equipe = {
         id: $stateParams.equipeId
      };

      $scope.delete = function(pel){
         console.log('delete pelada:', pel);
         if (confirm('Deseja realmente remover este item?')) {
            pel.$delete(function() {
               console.log('Pelada removida com sucesso');
               loadPelada();
            });
         }
      };

      var loadPelada = function() {
         $scope.peladas = Pelada.query();
      };

   })
   .controller('PeladaViewCtrl', function($scope, $rootScope, $state, $stateParams, Pelada, Equipe) {
      console.debug('PeladaViewCtrl id', $stateParams.id );
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onViewEnter($scope);
         loadPelada();

         $scope.show = "info";

      });

      var loadPelada = function() {
         Pelada.get({ id: $stateParams.id }).$promise.then(function(pelada){
            $scope.pelada = pelada;
            loadPeladeiros();
            console.debug('pelada:',$scope.pelada);
         });
      };
      var loadPeladeiros = function() {
         Equipe.get({id:$scope.pelada.equipe.id}).$promise.then(function(equipe){
            console.log(equipe.peladeiros);
            $scope.peladeiros = equipe.peladeiros;
         });
      };
      $scope.confirm = function(id){

         if(!$scope.pelada.confirmados){
            $scope.pelada.confirmados = [];
         }
         if( $scope.pelada.confirmados.indexOf(id)==-1) {
            $scope.pelada.confirmados.push(id);
            if($scope.pelada.rejeitados.indexOf(id)>-1) {
               $scope.pelada.rejeitados.splice($scope.pelada.rejeitados.indexOf(id), 1);
            }
            $scope.pelada.$save(function () {
               console.debug('Confirmação salvo!!!')
            });
         }else{
            console.log('Peladeiro já confirmado')
         }
      };

      $scope.refuse = function(id){

         if(!$scope.pelada.rejeitados){
            $scope.pelada.rejeitados = [];
         }
         if($scope.pelada.rejeitados.indexOf(id)==-1) {
            $scope.pelada.rejeitados.push(id);
            if($scope.pelada.confirmados.indexOf(id)>-1) {
               $scope.pelada.confirmados.splice($scope.pelada.confirmados.indexOf(id), 1);
            }
           $scope.pelada.$save(function () {
               console.debug('rejection    saved!!!')
            });
         }else{
            console.log('Peladeiro já rejeitou a pelada')
         }
      };

      $scope.unknown = function(id){

         if($scope.pelada.rejeitados.indexOf(id)>-1) {
            $scope.pelada.rejeitados.splice($scope.pelada.rejeitados.indexOf(id), 1);
         }
         if($scope.pelada.confirmados.indexOf(id)>-1) {
            $scope.pelada.confirmados.splice($scope.pelada.confirmados.indexOf(id), 1);
         }
         $scope.pelada.$save(function () {
            console.debug('unknown saved!!!')
         });
      }



      $scope.filterConfirmed = function (element) {
         console.log('novo' + $scope.pelada.confirmados);

         return $scope.pelada.confirmados.indexOf(element.id) > -1;
      };

      $scope.filterClose = function(element) {
         return $scope.pelada.confirmados.indexOf(element.id) == -1;
      }

   })
   .controller('PeladaInviteCtrl', function($scope, Search) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onActiveCtrl();
         $scope.$parent.isCreatePage = false;
      });

      $scope.data = { q: ''};
      $scope.search = function() {
         console.log('search q:', $scope.data.q);
         Search.search({
            index:'pelada',
            type:'peladeiro',
            body: ejs.Request().query(ejs.CommonTermsQuery('_all', $scope.data.q))
         }).then(function (body) {
                  $scope.results = body.hits.hits;
            console.log($scope.results);
         }, function (error) {
            console.trace(error.message);
         });
      };


   })
   .controller('PeladaCreateCtrl', function($scope, $rootScope, $state, $stateParams, Pelada, Local) {
      console.debug('PeladaCreateCtrl equipeId:',$stateParams.equipeId);
      $scope.$on('$ionicView.enter', function() {
         $scope.$parent.onCreateEnter()
      });

      $scope.pelada = new Pelada();
      $scope.options = {
         format: 'dd/mm/yyyy', // ISO formatted date
         onClose: function(e) {
         }
      };

      $scope.callbackMethod = function (query, isInitializing) {
         if(isInitializing || query.length < 3) {
            return [];
         } else {
            return  Local.query({where:{nome:{contains:query}}}).$promise;
         }
      };
      $scope.clickedMethod = function(callback){
         $scope.pelada.local = callback.item.id;
      };

      $scope.save = function() {
         console.debug('Adicionando nova Pelada...');
         $scope.pelada.data = new Date($scope.pelada.data);
         console.log($scope.pelada);
         $scope.pelada.$save(function(pelada) {
            console.debug('Pelada Sucess created, id:', pelada.id);
            $state.go('app.viewPelada', {id:pelada.id}); // on success go back to home i.e. equipe state.
         });
      };

   });
