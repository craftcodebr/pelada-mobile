/**
 * Created by icastilho on 12/21/15.
 */
angular.module('starter.controllers.local', [])
   .controller('LocaisCtrl', function ($scope, $stateParams, Local) {

      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onActiveCtrl();
         console.log('Peladas Entro');
      });

      Local.query().$promise.then(function (locais) {
         console.log(locais.length);
         $scope.locais = locais;
      });
   })
   .controller('LocalViewCtrl', function($scope, $rootScope, $state, $stateParams, $ionicLoading, Local) {
      console.debug('LocalViewCtrl id', $stateParams.id );
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onViewEnter($scope);
         $scope.$parent.clearAllFabs();
         $scope.map = null;
         $scope.loadLocal(); // Load a movie which can be edited on UI
      });

      $ionicLoading.show({
         template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Pesquisando Localização!'
      });

      $scope.loadLocal = function() { //Issues a GET request to /app/local/:id to get a local to update
         Local.get({ id: $stateParams.id }).$promise.then(function(local){
            $scope.local = local;
            console.debug($scope.local);

            createMap($scope, $scope.local.location.lat, $scope.local.location.lon);

            $ionicLoading.hide();
         });
      };

   })
   .controller('LocalCreateCtrl', function($scope, $state, $cordovaGeolocation, $ionicLoading, Local) {
      $scope.$on('$ionicView.enter', function() {
         $scope.map = null;
         $scope.local = new Local();
         $scope.$parent.onCreateEnter();

         ionic.Platform.ready(function(){

            console.log('ready...GO');

            $ionicLoading.show({
               template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Pesquisando Localização!'
            });

            var posOptions = {
               enableHighAccuracy: true,
               timeout: 20000,
               maximumAge: 0
            };

            $scope.data = {
               search:''
            };

            $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
               console.log('Current position');
               $scope.local.location = {
                  lat: position.coords.latitude,
                  lon:  position.coords.longitude
               };

               $scope.map = {
                  center: { latitude:  $scope.local.location.lat, longitude:  $scope.local.location.lon }, zoom: 16,
               };

               $scope.marker = {
                  coords: $scope.map.center,
                  id: 'localizacao'
               };

               autocomplete = new google.maps.places.Autocomplete(document.getElementById('search'));
               google.maps.event.addListener(autocomplete, 'place_changed', function() {
                  var place = autocomplete.getPlace();
                  $scope.local.location = {
                     lat: place.geometry.location.lat(),
                     lon: place.geometry.location.lng()
                  };
                  $scope.map.center = {latitude: $scope.local.location.lat, longitude:  $scope.local.location.lon};
                  $scope.marker.coords = $scope.map.center ;
                  $scope.$apply();
               });
               $ionicLoading.hide();
            }, function(err) {
               $ionicLoading.hide();
               console.log(err);
            });
         });

      });

      $scope.tipos = [
         {id:0, nome:'Quadra'},
         {id:1, nome:'Society'},
         {id:2, nome:'Campo'}
      ];

      $scope.save = function() {
         console.log($scope.local);

         $scope.local.$save(function(local) {
            console.debug('Local Sucess created, id:', local.id);
            $state.go('app.viewLocal', {id:local.id});
         });
      };


   });

function createMap($scope, lat, lon){
   $scope.map = null;
   console.log('createMap: ',lat, ' - ', lon );

   $scope.map = {
      center: { latitude: lat, longitude: lon }, zoom: 16,
   };

   $scope.marker = {
      coords: $scope.map.center,
      id: 'local'
   };


   /*var markerPoint = new google.maps.LatLng(lat, lon);
   var mapOptions = {
      center: markerPoint,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
   };

   $scope.map = new google.maps.Map(document.getElementById("mapLocal"), mapOptions);
   setMarker($scope, markerPoint);
   return markerPoint;*/
}
