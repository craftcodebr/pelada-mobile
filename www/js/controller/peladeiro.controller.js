/**
 * Created by icastilho on 12/17/15.
 */
angular.module('starter.controllers.peldeiro', [])

   .controller('PeladeiroCreateCtrl', function($scope, $rootScope, Peladeiro, Equipe,$ionicLoading  ) {
      $scope.$on('$ionicView.enter', function () {
         $scope.$parent.isCreatePage = true;
         $scope.$parent.onCreateEnter();
         $scope.newPeladeiro = new Peladeiro();
         $scope.equipes = Equipe.query({'lider.usuario.id':$rootScope.user.$id});
      });
      $rootScope.save = function(){
         $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Salvando ..!'
         });
         console.log('salvar peladeiro');
         console.log($scope.newPeladeiro.nome);
         $scope.newPeladeiro.$save(function(result) {
            console.debug('Peladeiro Sucess created, id:', result.id);
            $rootScope.$ionicGoBack();
            $ionicLoading.hide();
         });
      }

   })
   .controller('PeladeirosEquipeCtrl', function($scope, Peladeiro, Search, $ionicModal) {

      $scope.$on('$ionicView.enter', function() {
         console.log('Entro PeladeirosEquipeCtrl');
         $scope.$parent.onChild(function () {
            $scope.setOpenModal(function () {
               console.log('setOpnemodal');
               $scope.modal.show();
               $scope.search();
            });
         });
      });

      $scope.data = { q: ''};
      $scope.search = function() {
         console.log('search q:', $scope.data.q);
         Search.search({
            index:'pelada',
            type:'peladeiro',
            body: ejs.Request().query(ejs.CommonTermsQuery('_all', $scope.data.q))
         }).then(function (body) {
            $scope.results = body.hits.hits;
            console.log($scope.results);
         }, function (error) {
            console.trace(error.message);
         });
      };

      $scope.addPeladeiro = function(peladeiro, id){
         peladeiro.id = id;
         try {
            $scope.$parent.equipe.peladeiros.push(peladeiro);
            $scope.$parent.equipe.$save(function () {
               console.debug('Update add peladeiro finished!!!')
            });
         }catch(err){
            console.error("Peladeira já faz parte da equipe");
         }
      };

      $scope.removePeladeiros = function(index, id){
         angular.forEach($scope.$parent.equipe.peladeiros, function(item){
            if(item.id===id){
               $scope.$parent.equipe.peladeiros.splice($scope.$parent.equipe.peladeiros.indexOf(item), 1);
               $scope.$parent.equipe.$save(function() {
                  console.debug('Update remove peladeiro finished!!!');
                  return;
               });
            }
         })
      };

      $scope.isEquipe = function(id){
         var isEquipe = false;
         angular.forEach($scope.$parent.equipe.peladeiros, function(item){
            if(item.id===id) {
               isEquipe = true;
            }
         });
         return isEquipe;
      };

      createModal('equipe-add-peladeiro.html',$scope, $ionicModal );
   });


function createModal(modalName, $scope, $ionicModal){
   $ionicModal.fromTemplateUrl(modalName, {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {
      $scope.modal = modal;
   });
   $scope.openModal = function() {
      $scope.modal.show();

   };
   $scope.closeModal = function() {
      $scope.modal.hide();
   };
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });
}