/**
 * Created by icastilho on 10/15/15.
 */
angular.module('starter.controllers.equipe', [])

   .controller('EquipesCtrl', function ($scope, $rootScope, $timeout, $state, popupService, ionicMaterialMotion, ionicMaterialInk, Equipe) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onActiveCtrl();
         $scope.equipes = Equipe.query({'lider.usuario.id':$rootScope.user.$id});
      });
      <!-- Visual Effects end -->

       //fetch all equipes. Issues a GET to /app/equipes

      $scope.deleteEquipe = function(equipe) { // Delete a equipe. Issues a DELETE to /app/equipes/:id
         if (popupService.showPopup('Really delete this?')) {
            equipe.$delete(function() {
               $state.go('app.equipes'); //redirect to equipes
            });
         }
      };


   }).controller('EquipeCreateCtrl', function($scope, $rootScope, $state, $stateParams, Equipe ) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onCreateEnter()
      });
      $scope.equipe = new Equipe();  //create new equipe instance. Properties will be set via ng-model on UI
      $scope.equipe.peladeiros = [];

      $rootScope.save = function() {
         console.log('save...');
            $scope.addEquipe();
      };

      $scope.addEquipe = function() {//create a new equipe. Issues a POST to /app/equipe
         console.debug('Adicionando Equipe...');
         $scope.equipe.lider = $rootScope.peladeiro.id;
         $scope.equipe.peladeiros.push($rootScope.peladeiro);
         $scope.equipe.$save(function(equipe) {
            console.debug('Equipe Sucess created, id:', equipe.id);
            $state.go('app.viewEquipe', {id:equipe.id}); // on success go back to home i.e. equipe state.
         });
      };

   }).controller('EquipeEditCtrl', function($scope, $rootScope, $state, $stateParams, Equipe) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onCreateEnter();
      });

      $scope.updateEquipe = function() { //Update the edited equipe. Issues a PUT to /app/equipes/:id
         console.debug('Update Equipe');
         $scope.equipe.$save(function() {
            $state.go('equipes'); // on success go back to home i.e. equipe state.
         });
      };

      $scope.loadEquipe = function() { //Issues a GET request to /app/equipes/:id to get a equipe to update
         $scope.equipe = Equipe.get({ id: $stateParams.id });
      };

      $scope.loadEquipe(); // Load a movie which can be edited on UI

   }).controller('EquipeViewCtrl', function($scope, $stateParams, Equipe) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onViewEnter();
      });

      Equipe.get({ id: $stateParams.id }).$promise.then(function(equipe) {
         $scope.equipe = equipe;
         $scope.peladeiros = equipe.peladeiros;
         console.log(equipe.peladeiros);
      });

   });

