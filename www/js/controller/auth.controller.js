/**
 * Created by icastilho on 10/2/15.
 */
angular.module('starter.controllers.auth', [])

   .controller('LoginCtrl', function($scope,$rootScope,$q, $state,$location,$ionicLoading, $timeout, AuthService, Auth, Usuario, Peladeiro,ionicMaterialInk) {
      console.log('LoginCtrl...');
      $scope.$parent.clearFabs();
      $timeout(function() {
         $scope.$parent.hideHeader();
      }, 0);
      ionicMaterialInk.displayEffect();

      $scope.doLoginPassword = function(authMethod) {
         console.log('Login by '+ authMethod);

         Auth.$authWithPassword({
            email    : $scope.username,
            password : $scope.password
         }, function(error, authData) {
            if (error) {
               console.log("Login Failed!", error);
            } else {
               console.log("Authenticated successfully with payload:", authData);
            }
         });
      };

      $scope.doLogin = function(authMethod) {
         console.log('Login by '+ authMethod);
         $ionicLoading.show({
            template: 'Loading...'
         });
         var state = 'app.profile.equipes';
         var auth = null;

         $timeout(function () {

            Auth.$authWithOAuthPopup(authMethod).then(function(authData) {
               console.log(authData);
               auth = authData;
               return Usuario.query({'uid': authData.uid}).$promise;

            }).then(function(data) {
               console.debug('Usuario:', data[0]);
               if(data[0])
                  return data[0];
               else {
                  return AuthService.createUser(auth);
               }
            }).then(function(ref) {
               console.log('It was Saved: ',ref);
               Peladeiro.query({'usuario':ref.id}).$promise.then(function(data){
                  $rootScope.peladeiro = data[0];
                  $rootScope.user = ref;
                  return;
               });
            }).catch(function(error) {
               if (error.code === 'TRANSPORT_UNAVAILABLE') {
                  Auth.authWithOAuthRedirect(authMethod)
                     //FIXME resolver melhor isso aqui
                     .then(function(authData, err) {
                        if (error) {
                           console.log("Login Failed!", err);
                        }else{
                           console.log('loged');
                           $state.go(state);
                        }
                     });
               } else {
                  console.log("Login Failed!", error);
                  alert('Ooops! Ocorreu algo muito inesperado ao tentar fazer seu login! Por Favor, tente novamente mais tarde!');
                  state = 'app.login';
               }
            }).finally(function() {
               console.log('Promisse finally');
               $state.go(state);
               $ionicLoading.hide();
            });

         }, 900);
      };

      Auth.$onAuth(function(authData) {
         console.log('onAuth');
         if (authData) {
            console.log("User " + authData.uid + " is logged in with " + authData.provider);
         } else{
            console.log("User is logged out");
            $state.go('app.login');
         }
      });
   })
   .controller('LogoffCtrl', function($scope, $state, Auth) {
      $scope.$on('$ionicView.enter',function() {
         console.log('Logoff ');
         Auth.$unauth();
         $state.go('app.login');
         $scope.$parent.clearAllFabs();
         $scope.$parent.hideHeader();
      });
   });
