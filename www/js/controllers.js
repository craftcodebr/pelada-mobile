angular.module('starter.controllers', ['starter.controllers.pelada', 'starter.controllers.auth', 'starter.controllers.equipe', 'starter.controllers.peldeiro', 'starter.controllers.local'])

   .controller('AppCtrl', function($scope, $timeout,ionicMaterialInk , ionicMaterialMotion) {

      // Form data for the login modal
      $scope.loginData = {};
      $scope.isExpanded = false;
      $scope.hasHeaderFabLeft = false;
      $scope.hasHeaderFabRight = false;
      $scope.isCreatePage = false;

      var navIcons = document.getElementsByClassName('ion-navicon');
      for (var i = 0; i < navIcons.length; i++) {
         navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
         });
      }

      ////////////////////////////////////////
      // Layout Methods
      ////////////////////////////////////////

      $scope.hideNavBar = function() {
         document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
      };

      $scope.showNavBar = function() {
         document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
      };

      $scope.noHeader = function() {
         var content = document.getElementsByTagName('ion-content');
         for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
               content[i].classList.toggle('has-header');
            }
         }
      };

      $scope.setExpanded = function(bool) {
         $scope.isExpanded = bool;
      };

      $scope.setHeaderFab = function(location) {
         var hasHeaderFabLeft = false;
         var hasHeaderFabRight = false;

         switch (location) {
            case 'left':
               hasHeaderFabLeft = true;
               break;
            case 'right':
               hasHeaderFabRight = true;
               break;
         }

         $scope.hasHeaderFabLeft = hasHeaderFabLeft;
         $scope.hasHeaderFabRight = hasHeaderFabRight;
      };

      $scope.hasHeader = function() {
         var content = document.getElementsByTagName('ion-content');
         for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
               content[i].classList.toggle('has-header');
            }
         }
      };

      $scope.hideHeader = function() {
         $scope.hideNavBar();
         $scope.noHeader();
      };

      $scope.showHeader = function() {
         $scope.showNavBar();
         $scope.hasHeader();
      };

      $scope.clearFabs = function() {
         var fabs = document.getElementsByClassName('button-fab');
         if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
         }
      };

      $scope.clearAllFabs = function() {
         var fabs = document.getElementsByClassName('button-fab');
         if (fabs.length && fabs.length > 0) {
            fabs[0].remove();
         }
      };

      $scope.setOpenModal = function(call){
         var fabs = document.getElementsByClassName('button-fab');
         if (fabs.length && fabs.length > 0) {
            fabs[0].onclick = call;
         }
      };

      $scope.onProfileCtrl = function(){
         // Set Header
         $scope.showHeader();
         $scope.clearFabs();
         $scope.isCreatePage = false;
         $scope.setExpanded(false);
         $scope.setHeaderFab(false);

         // Set Motion
         $timeout(function() {
            ionicMaterialMotion.slideUp({
               selector: '.slide-up'
            });
         }, 100);

         $timeout(function() {
            ionicMaterialMotion.fadeSlideInRight({
               startVelocity: 1000
            });
         }, 250);

         // Set Ink
         ionicMaterialInk.displayEffect();
      };

      $scope.onActiveCtrl = function() {
         $scope.showHeader();
         $scope.clearFabs();
         $scope.isCreatePage = false;
         $scope.setExpanded(false);
         $scope.setHeaderFab('right');
         $timeout(function() {
            ionicMaterialMotion.fadeSlideIn({
               selector: '.animate-fade-slide-in .item'
            });
         }, 250);
         // Activate ink for controller
         ionicMaterialInk.displayEffect();
      };

      $scope.onCreateEnter = function() {
         // Set Header
         $scope.showHeader();
         $scope.isCreatePage = true;
         $scope.isExpanded = false;
         $scope.setHeaderFab('left');

         // Delay expansion
         $timeout(function () {
            // Set Motion
            ionicMaterialMotion.fadeSlideInRight();

         }, 100);
         // Set Ink
         ionicMaterialInk.displayEffect();
         $scope.clearAllFabs();
      };

      $scope.onViewEnter = function() {
         <!-- Visual Effects start -->
         // Set Header
         $scope.isCreatePage = false;
         $scope.showHeader();
         $scope.setExpanded(false);
         $scope.setHeaderFab(false);

         // Set Motion
         $timeout(function() {
            ionicMaterialMotion.slideUp({
               selector: '.slide-up'
            });
         }, 100);

         // Set Ink
         ionicMaterialInk.displayEffect();
         <!-- Visual Effects end -->
         $scope.clearFabs();
      };

      $scope.onFriendsCtrl = function(){

         $scope.isCreatePage = false;
         $scope.showHeader();
         $scope.clearFabs();
         $scope.setHeaderFab('right');
         // Delay expansion
         $timeout(function() {
            $scope.setExpanded(false);
            // Set Motion
            ionicMaterialMotion.fadeSlideInRight();

         }, 100);
         // Set Ink
         ionicMaterialInk.displayEffect();
      };

      $scope.onChild = function(callback){
         $scope.setHeaderFab(true);
         $scope.clearFabs();
         callback();
      }

   })
   .controller('AtividadeCtrl', function($scope) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onActiveCtrl();
         console.log('Entro AtivonActiveCtrlidade')
      });
   })
   .controller('ProfileCtrl', function($scope, Pelada) {
      $scope.$on('$ionicView.enter', function(){
         $scope.$parent.onProfileCtrl();
         $scope.peladas = Pelada.query();
         console.log('Entro Profile')
      });


   })
   .controller('MapController', function($scope, $cordovaGeolocation, $ionicLoading) {

      $scope.$on('$ionicView.enter', function(){
         $scope.map = {};
         $scope.$parent.onCreateEnter();
         console.log('Entro map controller');

         ionic.Platform.ready(function(){
            $ionicLoading.show({
               template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
            });

            var posOptions = {
               enableHighAccuracy: true,
               timeout: 20000,
               maximumAge: 0
            };

            $scope.data = {
               search:''
            };


            $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
               var lat  = position.coords.latitude;
               var lon = position.coords.longitude;

               $scope.map = {
                  center: { latitude: lat, longitude: lon }, zoom: 16,
               };

               $scope.marker = {
                  coords: $scope.map.center,
                  id: 'localizacao'
               };

               autocomplete = new google.maps.places.Autocomplete(document.getElementById('search'));
               google.maps.event.addListener(autocomplete, 'place_changed', function() {
                  var place = autocomplete.getPlace();
                  console.log('listerner map:');
                  $scope.map.center = {latitude: place.geometry.location.lat(), longitude:  place.geometry.location.lng()};
                  $scope.marker.coords = $scope.map.center ;
                  $scope.$apply();
               });
               $ionicLoading.hide();
            }, function(err) {
               $ionicLoading.hide();
               console.log(err);
            });

         });
      });
   })
;

