/**
 * Created by icastilho on 10/25/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Usuario
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Usuario', ['ServiceUrl', '$resource', Usuario]);

   var path = '/usuario/:id';

   function Usuario(ServiceUrl, $resource) {
      // Expose the service object
      console.debug(ServiceUrl);
      return $resource(ServiceUrl.url+path, { id: '@id' }, {
         update: {
            method: 'PUT' // this method issues a PUT request
         }
      });

   }

})();