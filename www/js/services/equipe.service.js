/**
 * Created by icastilho on 9/30/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Equipe
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Equipe', ['ServiceUrl', '$resource', Equipe])
      .service('popupService', function ($window) {
         this.showPopup = function (message) {
            return $window.confirm(message);
         }
      });

   var path = '/equipe/:id';

   function Equipe(ServiceUrl, $resource) {
      // Expose the service object
      return $resource(ServiceUrl.url+path, { id: '@id' });

   }

})();