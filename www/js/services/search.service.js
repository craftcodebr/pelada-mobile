/**
 * Created by icastilho on 11/20/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Search
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Search', ['esFactory', Search]);

   function Search(esFactory) {
      return esFactory({
         host: 'https://paas:e6c81734e2d56b1ff18bcd2b60209a9c@dwalin-us-east-1.searchly.com',
         log: 'trace'
      });

   }

})();