/**
 * Created by icastilho on 10/2/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Auth
    *
    * @description
    * The module declaration for this service object.
    */

   function AuthService(Usuario) {

      // Expose the service object
      return {
         createUser: createUser,
         get: get
      };

      function createUser(authData) {
         console.log('Create User..');
         var usuario = new Usuario();
         usuario.uid = authData.uid;
         usuario.provider = authData.provider;

         switch (authData.provider) {
            case 'password':
               usuario.displayName = authData.password.email.replace(/@.*/, '');
               //usuario.profileImageURL = authData.password.profileImageURL;
               break;
            case 'twitter':
               usuario.displayName = authData.twitter.displayName;
               usuario.profileImageURL = authData.twitter.profileImageURL;
               break;
            case 'facebook':
               usuario.displayName =  authData.facebook.displayName;
                  usuario.profileImageURL = authData.facebook.profileImageURL;
               break;
            case 'google':
               usuario.displayName =  authData.google.displayName;
                  usuario.profileImageURL = authData.google.profileImageURL;
               break;
            case 'github':
               usuario.displayName = authData.github.displayName;
                  usuario.profileImageURL = authData.github.profileImageURL;
               break;
         }
         return usuario.$save();
      }

      function get(key) {
         console.log('peladakey');
      }
   }

   angular.module('starter')
      .constant('AuthEndpoint', {
         url:  'https://glowing-torch-1906.firebaseio.com'
      })
      .factory('AuthService', ['Usuario', AuthService])

      .factory('FireBaseRef', function (AuthEndpoint) {
         console.log('FireBaseRef factory');
         return  new Firebase(AuthEndpoint.url);
      })

      .factory('Auth', function ($firebaseAuth, FireBaseRef) {
         console.log('Auth factory');
         return $firebaseAuth(FireBaseRef);
      })

      .factory("UserFactory", function($firebaseObject) {
         return $firebaseObject.$extend({
            getFullName: function() {
               // concatenate first and last name
               return this.first_name + " " + this.last_name;
            },
            existe: function() {
               var b = false;
               if(this.profile)
                  b = true;
               return b;
            }
         });
      })

      .factory("UserService", function(UserFactory, AuthEndpoint) {
         return new Firebase(AuthEndpoint.url+"/users/");
      })

      .factory("User", function(UserFactory, AuthEndpoint) {
         var ref = new Firebase(AuthEndpoint.url+"/users/");
         return function(userid) {
            return new UserFactory(ref.child(userid));
         }
      });

})();