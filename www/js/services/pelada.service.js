/**
 * Created by icastilho on 9/30/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Pelada
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Pelada', ['ServiceUrl', '$resource', Pelada]);

   var path = '/pelada/:id';

   function Pelada(ServiceUrl, $resource) {
      // Expose the service object
      return $resource(ServiceUrl.url+path, { id: '@id' });
   }

})();