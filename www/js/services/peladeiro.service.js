/**
 * Created by icastilho on 10/25/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Peladeiro
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Peladeiro', ['ServiceUrl', '$resource', Peladeiro]);

   var path = '/peladeiro/:id';

   function Peladeiro(ServiceUrl, $resource) {
      // Expose the service object
      return $resource(ServiceUrl.url+path, { id: '@id' }, {
         update: {
            method: 'PUT' // this method issues a PUT request
         }
      });

   }

})();