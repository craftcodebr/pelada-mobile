/**
 * Created by icastilho on 12/21/15.
 */
(function() {
   'use strict';

   /**
    * @ngdoc module
    * @name Local
    *
    * @description
    * The module declaration for this service object.
    */
   angular.module('starter')
      .factory('Local', ['ServiceUrl', '$resource', Local])
      .service('popupService', function ($window) {
         this.showPopup = function (message) {
            return $window.confirm(message);
         }
      });

   var path = '/local/:id';

   function Local(ServiceUrl, $resource) {

      // Expose the service object
      return $resource(ServiceUrl.url+path, { id: '@id' });

   }

})();