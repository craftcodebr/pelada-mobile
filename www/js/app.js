// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ngCordova', 'ngResource', 'starter.controllers', 'ionic-material',
   'angular-datepicker', 'ionMdInput','firebase', 'elasticsearch', 'uiGmapgoogle-maps', 'ion-autocomplete'])

   .run(function($ionicPlatform, $rootScope,$state, $location, Auth, Usuario,Peladeiro) {
      $ionicPlatform.ready(function () {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

         }
         if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
         }
      });
      var authData = Auth.$getAuth();

      if (authData) {
         console.debug(" run User " +  authData.uid + " is logged in with " +  authData.provider);
         Usuario.query({uid:authData.uid}).$promise
            .then(function(data) {
               console.debug('Set rootScope: ', data[0]);
               console.debug(data[0].id);
               $rootScope.user = data[0];
               return  Peladeiro.query({'usuario':$rootScope.user.id}).$promise;
            }).then(function(peladeiro) {
               console.debug('Set peladeiro root: ', peladeiro[0]);
               $rootScope.peladeiro = peladeiro[0];
               console.log("pequipe");
               console.log(peladeiro);
            });

      } else {
         console.debug("run User is logged out");
         $location.path("/app/login");
      }

      $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
         console.debug('$routeChangeError');
         // We can catch the error thrown when the $requireAuth promise is rejected
         // and redirect the user back to the home page
         if (error === "AUTH_REQUIRED") {
            $location.path("/app/login");
         }
      });

      $rootScope.$state = $state;

   })
   .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider

         .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
         })
         .state('app.atividade', {
            url: '/atividade',
            views: {
               'menuContent': {
                  templateUrl: 'templates/atividade.html',
                  controller: 'AtividadeCtrl'
               }
            }
         })
         .state('app.map', {
            url: '/map',
            views: {
               'menuContent': {
                  templateUrl: 'templates/map.html',
                  controller: 'MapController'
               }
            }
         })
         .state('app.login', {
            url: '/login',
            views: {
               'menuContent': {
                  templateUrl: 'templates/login.html',
                  controller: 'LoginCtrl'
               },
               'fabContent': {
                  template: ''
               }
            }
         })
         .state('app.logoff', {
            url: '/logoff',
            views: {
               'menuContent': {
                  controller: 'LogoffCtrl'
               }
            }
         })
         .state('app.profile', {
            url: '/profile',
            views: {
               'menuContent': {
                  templateUrl: 'templates/profile.html',
                  controller: 'ProfileCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile" ui-sref="app.createEquipe" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
               }
            }
         })
         .state('app.profile.equipes', {
            url: '/equipes',
            views: {
               profileContent:{
                  templateUrl: 'templates/equipe/profile-equipes.html'
               }
            }
         })
         .state('app.profile.peladas', {
            url: '/peladas',
            views: {
               profileContent:{
                  templateUrl: 'templates/pelada/profile-peladas.html'
               }
            }
         })
         .state('app.equipes', {
            url: '/equipes',
            views: {
               'menuContent': {
                  templateUrl: 'templates/equipe/equipes.html',
                  controller: 'EquipesCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile" ui-sref="app.createEquipe" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
               }
            }
         })
         .state('app.viewEquipe', {
            url: '/equipes/:id/view',
            views: {
               'menuContent': {
                  templateUrl: 'templates/equipe/equipe-view.html',
                  controller: 'EquipeViewCtrl'
               }
            }
         })
         .state('app.viewEquipe.peladeiros', {
            url: '/peladeiros',
            views: {
               equipeContent:{
                  templateUrl: 'templates/peladeiro/equipe-peladeiros.html',
                  controller: 'PeladeirosEquipeCtrl'
               },
               'fabContent@app': {
                  template: '<button id="fab-profile"  class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-person-add"></i></button>',
                  controller: function ($timeout) {
                     $timeout(function () {
                        document.getElementById('fab-profile').classList.toggle('on');
                     }, 100);
                  }
               }
            }
         })
         .state('app.createEquipe', { //state for adding a new equipe
            url: '/equipes/create',
            views: {
               'menuContent': {
                  templateUrl: 'templates/equipe/equipe-create.html',
                  controller: 'EquipeCreateCtrl'
               },
            }
         }).state('app.editEquipe', { //state for updating a equipe
            url: '/equipes/:id/edit',
            views: {
               'menuContent': {
                  templateUrl: 'templates/equipe/equipe-edit.html',
                  controller: 'EquipeEditCtrl'
               }
            }
         })
         .state('app.peladeiros', {
            url: '/peladeiros',
            views: {
               'menuContent': {
                  templateUrl: 'templates/peladeiros.html',
                  controller: 'PeladeirosCtrl'
               }
            }
         })
         .state('app.peladeiro', {
            url: '/peladeiros/:peladeiroId',
            views: {
               'menuContent': {
                  templateUrl: 'templates/peladeiro.html',
                  controller: 'PeladeiroCtrl'
               }
            }
         })
         .state('app.createPeladeiro', {
            url: '/peladeiro/create',
            views: {
               'menuContent': {
                  templateUrl: 'templates/peladeiro/peladeiro-create.html',
                  controller: 'PeladeiroCreateCtrl'
               }
            }
         })
         .state('app.peladas', {
            url: '/peladas/:equipeId',
            views: {
               'menuContent': {
                  templateUrl: 'templates/pelada/peladas.html',
                  controller: 'PeladasCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile"  ui-sref="app.createPelada({equipeId:equipe.id})" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>'
               }
            }
         })
         .state('app.createPelada', {
            url: '/peladas/create/:equipeId',
            views: {
               'menuContent': {
                  templateUrl: 'templates/pelada/pelada-create.html',
                  controller: 'PeladaCreateCtrl'
               }
            }
         })
         .state('app.viewPelada', {
            url: '/peladas/:id/view',
            views: {
               'menuContent': {
                  templateUrl: 'templates/pelada/pelada-view.html',
                  controller: 'PeladaViewCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile"  ui-sref="app.invitePeladeiros({peladaId:pelada.id})" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-person-add"></i></button>'
               }
            }
         })
         .state('app.invitePeladeiros', {
            url: '/peladas/invite/:peladaId',
            views: {
               'menuContent': {
                  templateUrl: 'templates/pelada/pelada-invite.html',
                  controller: 'PeladaInviteCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile"  ui-sref="app.createPeladeiro" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>'
               }
            }
         })
         .state('app.locais', {
            url: '/local',
            views: {
               'menuContent': {
                  templateUrl: 'templates/local/locais.html',
                  controller: 'LocaisCtrl'
               },
               'fabContent': {
                  template: '<button id="fab-profile"  ui-sref="app.createLocal" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>'
               }
            }
         })
         .state('app.createLocal', {
            url: '/local/create',
            views: {
               'menuContent': {
                  templateUrl: 'templates/local/local-create.html',
                  controller: 'LocalCreateCtrl'
               }
            }
         })
         .state('app.viewLocal', {
            url: '/local/:id/view',
            views: {
               'menuContent': {
                  templateUrl: 'templates/local/local-view.html',
                  controller: 'LocalViewCtrl'
               }
            }
         });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/app/profile');
   })
   .config(function(uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
         key: 'AIzaSyDqURpxN56vzmTD6PinvfkSWPgnQPp4qyI',
         v: '3.20', //defaults to latest 3.X anyhow
         libraries: 'places,weather,geometry,visualization'
      });
   })
   .run(['$templateCache', function ($templateCache) {
      $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" placeholder="Search">');
      $templateCache.put('window.tpl.html', '<div ng-controller="WindowCtrl" ng-init="showPlaceDetails(parameter)">{{place.name}}</div>');
   }])
   .constant('ServiceUrl', {
      url:  'https://pelada-service.herokuapp.com'
      //url: 'http://localhost:1337'
   });

